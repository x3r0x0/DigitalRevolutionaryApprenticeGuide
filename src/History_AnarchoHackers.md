# The emerging of Anarcho hackers

The post hacker crackdown - After the comercialization and corporate appropiation of hacking. [The emerging of Anarcho hackers...]

The cyberpunk literature and the hacker ethics by Steven Levy influenced libertarian rebel tech kids that already had prevision visions of a totalitarian future society owned by governments and then by corporations. [[Hackers : heroes of the computer revolution]] Starting with 1984, blade runner, Mona Lisa overdrive, Neuromancer, hacker ethics and the introduction of ideology and political thinking in digital forms, Mikhail Bakunin, Peter Kropotkin, Emma Goldman, Catalonia, and CNT zapatistas. With this influence, and modernizing ideologies to modern day the ideals with free software, digital production of goods, open source, open hardware, Internet decentralization, privacy and autonomy, etc.

At some point the very anti state hackers found themselves reading deeper and finding true anarchist ideology and with it shifting to also confront capitalism, corporate power, and any form of oppression, leaving behind the simplistic hacker view of distrust for government from the late 1980's.

When did hackers start to distrust government and corporations? 1980's + hacker wars, FBI raids, and later the Crypto Wars pushing middle and working class hackers to get a deeper social-economic understanding of the world we live in. The parallel ramifications that somehow goes hand in hand cypher punk and crypto-anarchists movements etc. [[Forget far-right populism – crypto-anarchists are the new masters]] The participation of Anarchist Hackers, hacktivists, anonymous and such in the Occupy Movement mark the reference where anarchist hackers made a big difference for the first time on street not just internet activism specially at occupy Oakland and occupy San Francisco.

During the years many names in the tech world have presented manifestos of revolutionary politics regarding the hacker culture. In 2003, big names in the free software community like Eric B Raymon (with his Jargon files) or Eben Moglen (with his The dotCommunistManifesto), and later on Aaron Swartz wrote the Guerrila Open Access Manifesto. [[Aaron Swartz. "Guerilla Open Access Manifesto"]]

Out of the mention above on i2p some hacktivist decided to organize and write their own manifesto directly positioning an anarchist hacker manifesto, was the pioneer one made around when anonymous was born, a couple rewritten versions later and we have a 2018 one with updated content.

Several notorious groups and individuals have come out of this movement like riseup [[20]][[21]] and antisec also of course anarchist hacker spaces like noisebridge [[22]][[23]] in San Francisco and C-Base in Berlin among others.

There are many hidden anarchist hackers but Jeremy Hammond was the first self described Anarchist hacker to get caught [[24]][[25]].

Anarchist hackers first organized as such, and still roam the realms of the i2p darknet. This is when many got together between 2010 and 2011 to form the first collectives made of the exclusive participation by anarchist and other left leaning radicals.

- This is when the Anarchist hackers reached maturity -

2015 until today

There are TV shows like MrRobot[[26]] and person or collective called Phineas Fisher[[27]] engaging other hackers to follow anarchist hacker ideals and fight with the hackback[[28]] call to direct action. There are podcasts like eldesarmador, hackerñol, and System Fail. There are documentaries like [hack the system](https://sub.media/video/trouble-8-hack-the-system/), [hackitat](https://open.lbry.com/@Hackernol:7/HACKITAT---9-LAYERS-OF-POLITICAL-HACKING:5?r=ATqhndQnpGqW3Sy6LhGRDxqgEMbe8Ugv), [Guardians of the New World](https://lbry.tv/@Hackernol:7/Guardians-Of-The-New-World-(Hacking-Documentary)---Real-Stories-a(A)a-720p:0?r=ATqhndQnpGqW3Sy6LhGRDxqgEMbe8Ugv), and [The Hacker Wars](https://lbry.tv/@CypherPuNk3D:8/The-Hacker-Wars:f).

In the last three years, academic papers have been analyzing in a direct matter some of the ideals of technology with anarchism[[29]][[30]] that Anarchist Hackers have been writing since the early 90's.

At hacker conferences the chaos computer club has always been the spot for anarchist hackers, from the beginning the chaos computer club has had an affinity with left politics and anarchist flavors[[31]][[32]]. Lately a group of anarchist hackers started to organize under one international name called 1Komona[[33]] in honor of the area were CCC organized first.

[The emerging of Anarcho hackers...]: https://www.noisebridge.net/wiki/Anarchisthackers/
[Hackers : heroes of the computer revolution]: https://en.wikipedia.org/wiki/Hackers:_Heroes_of_the_Computer_Revolution
[Forget far-right populism – crypto-anarchists are the new masters]: https://www.theguardian.com/technology/2017/jun/04/forget-far-right-populism-crypto-anarchists-are-the-new-masters-internet-politics
[Aaron Swartz. "Guerilla Open Access Manifesto"]: http://archive.org/details/GuerillaOpenAccessManifesto
[20]: https://www.eff.org/deeplinks/2015/01/security-not-crime-unless-youre-anarchist
[21]: https://www.theverge.com/2014/4/15/5614652/deny-the-machine
[22]: http://newworker.co/mag/anatomy-anarchist-hackerspace/
[23]: https://www.youtube.com/watch?v=nsiYTBQpIJ8
[24]: https://www.huffingtonpost.com/2013/05/28/jeremy-hammond-anonymous-hacker-guilty-stratfor_n_3347215.html
[25]: https://abcnews.go.com/Technology/fbi-wanted-hacker-jeremy-hammond-cats-password/story?id=26884738
[26]: https://www.polygon.com/2015/5/27/8670419/watch-the-entire-season-premiere-of-hacker-anarchist-show-mr-robot
[27]: https://motherboard.vice.com/en_us/article/3k9zzk/hacking-team-hacker-phineas-fisher-has-gotten-away-with-it
[28]: https://pastebin.com/raw/cRYvK4jb
[29]: https://www.academia.edu/38324329/Reflections_on_Authoritarian_Populism_Democracy_Technology_and_Ecological_Destruction
[30]: https://www.academia.edu/964088/Anarchism_and_the_Politics_of_Technology
[31]: https://medium.com/@eirinimalliaraki/34c3-reflections-on-the-hacker-ethos-611f886b2a2d
[32]: https://hackstory.net/Chaos_Computer_Club
[33]: https://events.ccc.de/congress/2017/wiki/index.php/Assembly:1Komona
