# Operating System Choice

There are plenty of choices out there for operating systems, but my choice is Arch Linux which you can then install BlackArch tools into.

## BlackArch Install

This BlackArch setup will feature full disk encryption with logical drives using UEFI & GRUB. If you have older hardware, then find resources for legacy boot with GRUB. To begin, boot off the Arch basic install CD from their website.

#### Option Pre-Step
* **Optional Pre-Step** for maximum security: NOTE: It took days for this to finish on one of my 3TB mechanical SATA drives, so decide if you need or want to do this pre-step on your drive.

    ```markdown
	# cryptsetup open --type plain -d /dev/urandom /dev/sda to_wipe
    ( lsblk to check it's there )
    # dd if=/dev/zero of=/dev/mapper/to_wipe status=progress
    ( then we close temp LUKS container)
    # cryptsetup close to_wipe
	```

### Start Arch Installation

First setup Arch Linux. Refer to the Arch Wiki if needed.

1. Setup partitions, with the first for the UEFI System Partiton [ESP] & GRUB partition and the remainder for the rest:

    ```markdown
	# gdisk /dev/sda
	( type n, blank, blank, +1G, ef00 [ESP] )
	( then type n, blank, blank, blank, 8e00 [/] )
	( then w )
    ```

1. Format EFI System Partition (ESP) and check it with "lsblk -f":

    ```markdown
	# mkfs.fat -F32 /dev/sda1
    ```

1. Setup system Encryption with luks version 2 with aes-xts-plain64 cipher (change to your needs):

    ```markdown
    # cryptsetup luksFormat --type luks2 -c aes-xts-plain64 -v -y /dev/sda2
    ( type YES and choose passwd & write it down... there is no recovery )
    # cryptsetup luksOpen /dev/sda2 cryptroot
    ( Check its creation
    # cryptsetup luksDump /dev/sda2
    ```

1. Next setup the logical volume manager (LVM) & create the logical volumes (LVs):

    ```markdown
    # pvcreate /dev/mapper/cryptroot
    # vgcreate rootvg /dev/mapper/cryptroot
    # lvcreate -n tmp -L 16G rootvg
    # lvcreate -n swap -L 8G rootvg
    # lvcreate -n root -l 100%FREE rootvg
    ```

1. Check your LVM setup with:
    * pvs
    * vgs
    * lvs
    * lsblk

1. Create filesystems for LVs and check with "lsblk -f":

    ```markdown
	# mkfs.ext4 /dev/mapper/rootvg-tmp
	# mkfs.ext4 /dev/mapper/rootvg-root
	# mkswap /dev/mapper/rootvg-swap
	# swapon /dev/mapper/rootvg-swap
    ```

1. Mount LVs and ESP to get ready for install:

    ```markdown
    # mount /dev/mapper/rootvg-root /mnt
    # mkdir /mnt/{tmp,boot}
    # mount /dev/mapper/rootvg-tmp /mnt/tmp
    # mount /dev/sda1 /mnt/boot
    ```

1. Install base system.

    ```markdown
	# pacstrap /mnt base linux linux-firmware base-devel vim zsh lvm2 netctl cryptsetup dhcpcd man-pages links mkinicpio e2fsprogs
    ```
    * Dependant upon your CPU, install "intel-ucode" or "amd-ucode"
    * Install the package for your video card (or do that later)
    * configure your locale
    * configure your timezone
    * configure your hostname
    * install and configure sudo
    * add your user account
    * set root passwd

1. Generate file system tab (fstab) & chroot to /mnt:

    ```markdown
    # genfstab -p /mnt > /mnt/etc/fstab ( Generate fstab file )
    # arch-chroot /mnt
    ```

1. Add some pacman fun:

    ```markdown
    # vim /etc/pacman.conf
    ( Under [options] section:
    ( uncomment "Color"
    ( Add "ILoveCandy" below it
    ( Enable Multilib
  
    ( Then ReSynchronize repositories:
    # pacman -Sy
    ```

1. Configure boot environment by configuring hooks for initramfs:

    ```markdown
    # vim /etc/mkinitcpio.conf
    ( add keyboard & keymap before autodetect
    ( HOOKS=( ... keyboard keymap autodetect ...)
    ( then add encrypt & lvm2 before filesystems
    ( HOOKS=( ... encrypt lvm2 filesystems ...)
    # mkinitcpio -p linux
    ```

1. Install GRUB bootloader:

    ```markdown
    # pacman -S grub efibootmgr
    ```

1. Configure GRUB:

    ```markdown
    ( Create bootloader entry: )
    # vim /etc/default/grub
    ( uncomment GRUB_ENABLE_CRYPTODISK=y )
    ( change GRUB_CMDLINE_LINUX="" to
    ( GRUB_CMDLINE_LINUX="cryptdevice=/dev/sda2:cryptroot root=/dev/mapper/rootvg-root" 
	( Then setup GRUB: )
	# grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB --recheck
	# grub-mkconfig -o /boot/grub/grub.cfg
    ```

1. Finish installation & reboot into new system:

    ```markdown
    # exit
    # umount -R /mnt
    # reboot
    ```

### X Windows Setup

Here is another choice as there are many different options for your X windows setup. I know lots of people enjoy the i3wm "windows manager", so I will use that, but you can choose any you want (OpenBox and others).

#### Install i3wm windows manager

Use the i3wm Arch wiki or the project's website if needed.

```markdown
$ sudo pacman -S xorg i3-wm lxdm i3blocks i3lock i3status dmenu rxvt-unicode xterm
```

#### Enable Display/Login Manager

Then we enable the LXDM Display/Login Manager for X.

```markdown
$ sudo systemctl enable lxdm.service
```

Then setup your X Windows environment to your needs.

#### BlackArch Install

Last, we install the BlackArch tools.

```markdown
$ curl -O https://blackarch.org/strap.sh
( The SHA1 sum should match: 9c15f5d3d6f3f8ad63a6927ba78ed54f1a52176b strap.sh )
$ sha1sum strap.sh
$ chmod +x strap.sh
$ sudo ./strap.sh
```

You may now install tools from the blackarch repository with:

```markdown
( To list all of the available tools, run
$ sudo pacman -Sgg | grep blackarch | cut -d' ' -f2 | sort -u

( To install all of the tools, run
$ sudo pacman -S blackarch

( To install a category of tools, run
$ sudo pacman -S blackarch-<category>

( To see the blackarch categories, run
$ sudo pacman -Sg | grep blackarch
```

### Credits

Gratitude to all the amazing gatos y gatas. Enjoy life & hack the system! ^_^
- killab33z
