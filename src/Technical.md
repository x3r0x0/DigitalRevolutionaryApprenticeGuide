# Technical

* Privacy
  - Software
    - Operating Systems
      - [BlackArch](./OS_blackarch.md)
      - tails
      - Qubes
      - Whonix
  - Hardware
* Research and gathering information Techniques 
  - OSINT
  - Dumpster diving and Trashing
  - Social Engineering
  - Coffee shop cam
  - Drones
  - Disturbance
  - Alchool
      - Gather at employees bar/club
      - Use drunk budy
      - cheatchat
  - Inside job or informant
    - Fustrated worker venting
    - Payment informant
  - Social Media
    - Fake hot woman/man profile
    - Fake profile of person of interest of target
      - hobby ( Scubba diving ) 
      - job related ( Linux Advocate )
* Explotation Techniques
  - 101 Binary Exploitation
  - [Offensive cheat sheet, commands and tips](./CheatSheet.md)
* Botnets
* Warez

