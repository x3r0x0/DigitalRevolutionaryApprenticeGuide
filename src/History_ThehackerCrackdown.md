# The hacker Crackdown

Video Short documentary on the topic: [The hacker Crackdown by The Hated One]

An 18-month long collaborative effort of the United States Secret Service and broken-up AT&T to take control of the Internet in the early 1990s. They struck right at the heart of what makes the Internet such a powerful tool - the hacker culture. Hackers were people who believed access to the Internet and computer technology should be as free and open as possible. They believed cyberspace was a not place for governments to censor and monitor. It was not a place for corporations to control and manipulate. For hackers, cyberspace was a new frontier that would free the mind and open endless opportunities. The Hacker Crackdown was the United States government ideological dirty war to take control of the digital realm. This is a story of people, for whom cyberspace was a war zone.

Government surveillance. Corporate censorship. Media manipulation. This is the reality of the Internet today. Bit by bit, multi-billion-dollar corporations are taking control of what we can do, see, and say on the cyberspace. We can’t make comments on social media, chat with people, or browse the web, without the government somewhere taking a copy in a rogue database dully paid for by us.

It’s almost as if there was a waging war for this huge digital space that was once dreamed of as setting people free. A frontier where everybody could unleash their potential. A true equality of opportunities. And it seems governments and big corporations hate this empowerment of the little ones. They despise it so much they work hand-in-hand to attack with full force in order to deny us any expansion of Constitutional rights into the digital realm. And it all started at the dawn of the commercial Internet with a drastic crackdown on their most feared opposition: hackers. The Hacker Crackdown of 1990 set a precedent for much of what is happening today.

I believe standing up against power and illegitimate authority is a moral duty. I believe all humans are fundamentally free. But this freedom won't take care of itself.

[The hacker Crackdown by The Hated One]: https://www.youtube.com/channel/UCjr2bPAyPV7t35MvcgT3W8Q
